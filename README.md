# search-launch-page

Basic little launch page for your favorite search engine searches. This just uses the `yaml` config to generate some web forms for commonly used searches.

![](slp-example.png)

## Configuration

Create a `yaml` file with a title and at least one search engine:

```yaml
title: the name of your page

searches:
    - name: ddg
      base_url: https://duckduckgo.com
      search_var: q  # the URL variable the search engine uses for the query
```

# Using the docker image

```
docker run --build-arg config_yaml=my_config.yaml docker.io/sivy0x0/search-launch-page
```