import click
from jinja2 import Environment, FileSystemLoader
from ruamel.yaml import YAML


@click.group
def cli():
    pass


def load_conf(f):
    yaml = YAML(typ="safe")
    return yaml.load(open(f, "r"))


@cli.command
@click.option("--config", default="q.yaml")
@click.option("--file", default="index.html")
def generate(file, config):
    conf = load_conf(config)
    for v in conf["searches"]:
        if "other_params" not in v:
            v["other_params"] = {}

    env = Environment(loader=FileSystemLoader("templates"))
    tmpl = env.get_template("index.html")
    with open(file, "w") as outfile:
        outfile.write(tmpl.render(**conf))


if __name__ == "__main__":
    cli()
