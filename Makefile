VENV := .venv
VENV_BIN := .venv/bin
PIP := $(VENV_BIN)/pip
PIPC := $(VENV_BIN)/pip-compile
PYTHON := $(VENV_BIN)/python

## python tools
$(VENV):
	python3 -m venv $(VENV)
	$(PIP) install --upgrade pip
	$(PIP) install pip-tools

.PHONY: clean_venv
clean_venv:
	rm -fr .venv

requirements.in:
	touch $@

requirements.txt: $(VENV) requirements.in
	$(PIPC) --quiet --output-file requirements.txt requirements.in

requirements-dev.txt: $(VENV) requirements.txt
	$(PIPC) --quiet --output-file requirements-dev.txt requirements-dev.in

.PHONY: install_requirements
install_requirements: requirements.txt
	$(PIP) install -r requirements.txt

.PHONY: install_requirements_dev
install_requirements_dev: requirements-dev.txt
	$(PIP) install -r requirements-dev.txt

.PHONY: clean_requirements
clean_requirements:
	rm -f requirements.txt requirements-dev.txt

index.html: templates/index.html
	python q.py --generate